#!/bin/bash

# Repository URL
REPO_URL='git@example.com:org/repo.git'

# Directory to store repositories
WORK_DIR='/home/www'

# Prefix for stored directories
DIR_PREFIX='project'

# Where to symlink the repository to
TARGET_DIR='/var/www/project'

# Which branch to deploy from the repository
DEPLOY_BRANCH='master'

# Function to run before symlinking
BEFORE_DEPLOY() {
    composer install --no-dev --optimize-autoloader
    npm ci
    ln -f -s "$HOME/.env" .env
    rm -rf storage
    ln -f -s "$HOME/storage" storage
    php artisan migrate --force
    npm run production
}

# Function to run after symlinking
AFTER_DEPLOY() {
    php artisan config:cache
    # php artisan route:cache
    service php7.2-fpm reload
}
