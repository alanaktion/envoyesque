# envoyesque

A minimal open-source implementation of Envoyer's build process

# Requirements

- Git 1.7.10 or later

# Setup

Copy `.config.example.sh` to `config.sh` and add your configuration.

By default, it includes example scripts for deploying a Laravel application, and expects `php`, `npm`, and `composer` to be installed in `/usr/bin`.

# Usage

Just run `deploy.sh` to deploy the latest commit!

# Tips

## Clearing OpCache

If you want to reload PHP-FPM after a deployment, make sure your sudoers file allows the user running `deploy.sh` to do so:

```
www ALL=NOPASSWD: /usr/sbin/service php7.2-fpm reload
```

If you're running systemd, this may need additional configuration change, and isn't ideal. You may have more success using something like [cachetool](http://gordalina.github.io/cachetool/) to connect to the FPM instance.
